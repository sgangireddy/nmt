# README #

The task is to reverse the given sentences using deep learning methods. In other terms the task is similar to machine translation task.

### Data###

*Sentences.txt* consists of required sentences for the task. Data splits for source and target:

	data/train.source and data/train.target : first 1800 sentences
	data/valid.source and data/valid.target : 1801 to 2000 sentences
	data/test.source and data/test.target :  remaining 342 sentences
	
The Architecture used for encoder and decoder:
	
	Both the encoder and decoder consists of one LSTM layer
	Before hidden layer, an embedding layer is used to learn the embeddings for both source and target words
	Optimizer: Adam optimizer

Since target sentences are reverse of source sentences same vocabulary is used for both encoder and decoder.

Here I used simple architecture (one LSTM layer) but a better machine translation system can be obtained with more LSTM layers, bi-directional LSTM layers and attention mechanism during decoding.

To run the system:

	python scripts/nmt_model.py data/train.source data/train.target data/vocab.source 32
	
	epoch: 0, train and valid costs:, 8.56764, 8.54618
	epoch: 1, train and valid costs:, 8.20126, 7.39930
	epoch: 2, train and valid costs:, 6.78193, 6.09823
	epoch: 3, train and valid costs:, 5.83096, 5.37669
	epoch: 4, train and valid costs:, 5.31826, 4.96507
	epoch: 5, train and valid costs:, 5.03837, 4.73581
	epoch: 6, train and valid costs:, 4.89936, 4.62779
	epoch: 7, train and valid costs:, 4.84148, 4.58633
	epoch: 8, train and valid costs:, 4.81980, 4.57402
	epoch: 9, train and valid costs:, 4.81120, 4.57335

