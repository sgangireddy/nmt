import sys

f = open(sys.argv[1], 'r')
vocab = {}

for line in f:
	line = line.strip().split()
	for word in line:
		if not vocab:
			vocab[word] = 1
		elif word in vocab:
			continue
		else:
			vocab[word] = 1

for key in vocab:
	print key
