import sys

f1 = open(sys.argv[1], 'r')
f2 = open(sys.argv[2], 'r')

vocab = {}
for line in f1:
	line = line.strip().split()
	for word in line:
		vocab[word] = 1

oov_count = 0
for line in f2:
	line = line.strip().split()
	for word in line:
		if not word in vocab:
			oov_count += 1

print oov_count
