import sys
import tensorflow as tf
import pdb
import numpy
#from tf.contrib.seq2seq import BasicDecoder, BeamSearchDecoder, TrainingHelper, dynamic_decode
#from tf.nn import dynamic_rnn, embedding_lookup
#from tf.nn.rnn_cell import BasicLSTMCell, BasicRNNCell
from tensorflow.python.layers import core


source_tfile = sys.argv[1]
target_tfile = sys.argv[2]

vocab_file = sys.argv[3]

batch_size = int(sys.argv[4])

v_f = open(vocab_file, 'r')

source_vfile = "data/valid.source"
target_vfile = "data/valid.target"

vocab = {}
count = 0

for line in v_f:
	line = line.strip()
	vocab[line] = count
	count += 1

	
e_embed_size = 50
d_embed_size = 50


e_num_hidden = 1
d_num_hidden = 1

e_num_hidden_neu = 100
d_num_hidden_neu = 100

e_vocab_size = len(vocab)
d_vocab_size = len(vocab)

learning_rate = 0.0001
training_epochs = 10

e_inputs = tf.placeholder(tf.int32, [None, batch_size]) #inputs to the encoder
d_inputs = tf.placeholder(tf.int32, [None, batch_size])	#inputs to the decoder
d_outputs = tf.placeholder(tf.int32, [None, batch_size]) 
e_seq_len = tf.placeholder(tf.int32, [batch_size]) #encoder sequence lengths, useful to reduce the unnecessary computations
d_seq_len = tf.placeholder(tf.int32, [batch_size]) #decoder sequence lengths
d_labels = tf.placeholder(tf.int32, [None, batch_size]) #target labels

e_embed_matrix = tf.get_variable("e_embed_matrix", [e_vocab_size, e_embed_size]) #encoding embedding matrix
d_embed_matrix = tf.get_variable("d_embed_matrix", [d_vocab_size, d_embed_size]) #decoding embedding matrix
#p_layer = tf.layers.dense(d_vocab_size, use_bias=False) # decoder output layer, before softmax

e_embed_lookup = tf.nn.embedding_lookup(e_embed_matrix, e_inputs) #encoder embeddings lookup
d_embed_lookup = tf.nn.embedding_lookup(d_embed_matrix, d_inputs) #decoder embeddings lookup

#encoder
e_hidden_layer = tf.nn.rnn_cell.BasicLSTMCell(e_num_hidden_neu) #encoder LSTM layer
e_o, e_s = tf.nn.dynamic_rnn(e_hidden_layer, e_embed_lookup, sequence_length = e_seq_len, dtype=tf.float32, time_major = True) # encoder rnn hidden state computations

#decoder
d_hidden_layer = tf.nn.rnn_cell.BasicLSTMCell(d_num_hidden_neu) #decoder LSTM layer
d_helper  =  tf.contrib.seq2seq.TrainingHelper(d_embed_lookup, d_seq_len, time_major = True) 

#Decoding
basic_decode = tf.contrib.seq2seq.BasicDecoder(d_hidden_layer, d_helper, e_s, output_layer=core.Dense(d_vocab_size, use_bias=False))
outputs, _, _= tf.contrib.seq2seq.dynamic_decode(basic_decode) # softmax output of the decoder
logits = outputs.rnn_output

cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=d_labels, logits=logits)) #computation of cross entropy
compute_grad = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost) #Adam optimizer, which compute the gradients and update the weights


#data provider for training
def get_batch(s_t, t_t, batch_size):
	s_sen, t_sen = [], []
	e_sl, d_sl = [], []
	e_i, d_i, d_l = [], [], []
	count = 0
	for line in s_t:
		if (count < batch_size):
			s_sen.append(line.strip())
			e_sl.append(len(line.strip().split())+2)
			count += 1
		else:
			break
	count = 0
	for line in t_t:
		if (count < batch_size):
			t_sen.append(line.strip())
			d_sl.append(len(line.strip().split())+2)
			count += 1
		else:
			break

	max_e_sl = max(e_sl) # in the current batch maximum sentece legth
	max_d_sl = max(d_sl)
	for sen_ in s_sen:
		wl_ = []
		wl_.append(vocab['<s>']) # adding sentence start symbol
		sen_ = sen_.strip().split()
		for w_ in sen_:
			if w_ in vocab:
				wl_.append(vocab[w_])
			else:
				wl_.append(vocab['<unk>']) # if the word is not in the list then that word is replaced with <unk>
		wl_.append(vocab['</s>']) # addding sentence end symbol
		while(len(wl_) < max_e_sl): wl_.append(vocab['</s>']) 
		e_i.append(wl_)
	
	for sen_ in t_sen:
		wl_ = []
		wl_.append(vocab['<s>'])
		sen_ = sen_.strip().split()
		for w_ in sen_:
			if w_ in vocab:
				wl_.append(vocab[w_])
			else:
				wl_.append(vocab['<unk>'])
		wl_.append(vocab['</s>'])
		while(len(wl_) < max_d_sl): wl_.append(vocab['</s>'])
		d_i.append(wl_)

	e_i = numpy.array(e_i) #converting list of lists to numpy array
	e_i = e_i[:, :e_i.shape[1]-1]
	e_i = numpy.transpose(e_i)

	d_i = numpy.array(d_i)
	td_i = d_i
	d_i = d_i[:, :d_i.shape[1]-1]
	d_i = numpy.transpose(d_i)
	d_l = td_i[:,1:]
	d_l = numpy.transpose(d_l)
	return s_t, t_t, e_i, d_i, e_sl, d_sl, d_l
	
		
initialize = tf.global_variables_initializer()

with tf.Session() as sess:
    	sess.run(initialize)
	for epoch in range(training_epochs):
		t_avg_cost = 0.
		s_t = open(source_tfile, 'r')
		t_t = open(target_tfile, 'r')
		t_num_batches = 0	
		while 1:
			#e_i : contains encoder input labels
			#d_i : contains decoder input labels
			#e_sl : encoder sequence lenghts
			#d_sl : decoder sequence lengths
			#d_l : decoder output labels
			s_t, t_t, e_i, d_i, e_sl, d_sl, d_l = get_batch(s_t, t_t, batch_size)
			e_sl[:]=[x-1 for x in e_sl]
			d_sl[:]=[x-1 for x in d_sl]
			if not e_i.shape[1] == batch_size:
				break
			else:
				t_num_batches += 1
			_, centropy = sess.run([compute_grad, cost], feed_dict={e_inputs: e_i, d_inputs: d_i, e_seq_len: e_sl, d_seq_len: d_sl, d_labels: d_l})
			t_avg_cost += centropy
		s_t.close()
		t_t.close()
		#print "epoch: %d, train cost:, %.5f" % (epoch, t_avg_cost/num_batches)
	
		v_avg_cost = 0.
		v_num_batches = 0	
		s_v = open(source_vfile, 'r')
                t_v = open(target_vfile, 'r')
		while 1:
			#e_i : contains encoder input labels
			#d_i : contains decoder input labels
			#e_sl : encoder sequence lenghts
			#d_sl : decoder sequence lengths
			#d_l : decoder output labels
			s_t, t_t, e_i, d_i, e_sl, d_sl, d_l = get_batch(s_v, t_v, batch_size)
			e_sl[:]=[x-1 for x in e_sl]
			d_sl[:]=[x-1 for x in d_sl]
			if not e_i.shape[1] == batch_size:
				break
			else:
				v_num_batches += 1
			centropy = sess.run([cost], feed_dict={e_inputs: e_i, d_inputs: d_i, e_seq_len: e_sl, d_seq_len: d_sl, d_labels: d_l})
			v_avg_cost += centropy[0]
		s_v.close()
		t_v.close()
		print "epoch: %d, train and valid costs:, %.5f, %.5f" % (epoch, t_avg_cost/t_num_batches, v_avg_cost/v_num_batches)

	
